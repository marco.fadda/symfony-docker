user nginx;
worker_processes auto;
pcre_jit on;
error_log /var/log/nginx/error.log warn;
daemon off;
pid /var/run/nginx.pid;

include /etc/nginx/modules/*.conf;

events {
     worker_connections 1024;
}

http {
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    server_tokens off;
    server_names_hash_bucket_size 64;
    server_name_in_redirect off;

    client_max_body_size 32m;
    keepalive_timeout 65;

    sendfile on;
    tcp_nodelay on;
    tcp_nopush on;

    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:SSL:2m;

    gzip on;
    gzip_vary on;
    gzip_static on;

    proxy_read_timeout 2m;
    proxy_send_timeout 2m;

    charset UTF-8;

    log_format main '$remote_addr - $remote_user [$time_local] "$request" '
			'$status $body_bytes_sent "$http_referer" '
			'"$http_user_agent" "$http_x_forwarded_for"';

    access_log /var/log/nginx/access.log main;

    server {
        listen 80 default_server;
        listen [::]:80 default_server;

        root /var/www/public;
        index index.php;

        location / {
            try_files $uri $uri/ /index.php$is_args$args;
        }

        location ~ ^/index\.php(/|$) {
            try_files $uri =404;
            fastcgi_pass unix:/var/run/php7-fpm.sock;
            fastcgi_index index.php;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;

            fastcgi_param  QUERY_STRING       $query_string;
            fastcgi_param  REQUEST_METHOD     $request_method;
            fastcgi_param  CONTENT_TYPE       $content_type;
            fastcgi_param  CONTENT_LENGTH     $content_length;

            fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
            fastcgi_param  SCRIPT_FILENAME    $realpath_root$fastcgi_script_name;
            fastcgi_param  REQUEST_URI        $request_uri;
            fastcgi_param  DOCUMENT_URI       $document_uri;
            fastcgi_param  DOCUMENT_ROOT      $realpath_root;
            fastcgi_param  SERVER_PROTOCOL    $server_protocol;
            fastcgi_param  REQUEST_SCHEME     $scheme;
            fastcgi_param  HTTPS              $https if_not_empty;

            fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
            fastcgi_param  SERVER_SOFTWARE    nginx;

            fastcgi_param  REMOTE_ADDR        $remote_addr;
            fastcgi_param  REMOTE_PORT        $remote_port;
            fastcgi_param  SERVER_ADDR        $server_addr;
            fastcgi_param  SERVER_PORT        $server_port;
            fastcgi_param  SERVER_NAME        $server_name;

            internal;
        }
    }
}
